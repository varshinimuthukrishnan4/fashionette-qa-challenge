package com.skava.frameworkutils;

        import java.io.FileInputStream;
        import java.io.FileOutputStream;
        import org.apache.poi.hssf.usermodel.HSSFWorkbook;
		import org.apache.poi.ss.usermodel.Cell;
		import org.apache.poi.ss.usermodel.DataFormatter;
		import org.apache.poi.ss.usermodel.Row;
		import org.apache.poi.ss.usermodel.Sheet;
		import org.apache.poi.ss.usermodel.Workbook;
     	import org.apache.poi.xssf.usermodel.XSSFWorkbook;
 
    public class ExcelUtils {
 
    	private static Sheet genericExcelWSheet;
        private static Workbook genericExcelWBook;
        private static Cell genericCell;
        private static Row genericRow;
        
        private static String path_TestData;
        
        private static boolean isHSSF=true;
 
    		//This method is to set the File path and to open the Excel file, Pass Excel Path and Sheetname as Arguments to this method
 
    	public static void setExcelFile(String path,String sheetName) throws Exception  {
    		
    		FileInputStream excelFile=null;
    		
    		try 
       			{
 
           			//set the Excel path to further xls file operation
       				path_TestData=path;
       				// Open the Excel file
       								
       				excelFile = new FileInputStream(path);
 
					if(path.endsWith(".xls"))
					{
						// Access the required test data sheet
						genericExcelWBook=new HSSFWorkbook(excelFile);
						genericExcelWSheet=genericExcelWBook.getSheet(sheetName);
						isHSSF=true;
					}
					else if(path.endsWith(".xlsx"))
					{
						genericExcelWBook=new XSSFWorkbook(excelFile);
						genericExcelWSheet=genericExcelWBook.getSheet(sheetName);
						isHSSF=false;
					}
					excelFile.close();
				} 
       			catch (Exception e){
 
						throw (e);
       			}
    			finally
    			{
    				if(excelFile !=null)
    				{
    					excelFile.close();
    				}
    			}
       			 
			}
    		
    	public static void setExcel(String excelPath) throws Exception
    		{
    			FileInputStream excelFile=null;
    			try
    			{
	    			//this method used to set the excel file only. Not sheet
	    			//set the Excel path to further xls file operation
    				path_TestData=excelPath;
	   				// Open the Excel file
	
					excelFile = new FileInputStream(excelPath);
	
					// Access the required test data sheet
					if(excelPath.endsWith(".xls"))
						genericExcelWBook = new HSSFWorkbook(excelFile);
					else
					{
						genericExcelWBook = new XSSFWorkbook(excelFile);
						isHSSF=false;
					}	
    			}
    			catch(Exception e)
    			{
    				throw (e);
    			}
    			finally
    			{
    				if(excelFile!=null)
    				{
    					excelFile.close();
    				}
    			}
    		}
    		
    	public static void setExcelSheet(String sheetName)
    		{
    			try
    			{
    				//this method used to set particular sheet name.
    				// Access the required test data sheet
    				genericExcelWSheet = genericExcelWBook.getSheet(sheetName);
    			}
    			catch(Exception e)
    			{
    				e.printStackTrace();
    			}
    		}
 
    		//This method is to read the test data from the Excel cell, in this we are passing parameters as Row num and Col num
 
    	public static String getCellData(int rowNum, int colNum) throws Exception
    	{
    	String cellData="";
    	 try{
    		 
    		genericCell = genericExcelWSheet.getRow(rowNum).getCell(colNum);
          			
          		switch (genericCell.getCellType())
          		{
    			 	case 0:
    			 		//This for numeric
    			 		cellData=""+(long)genericCell.getNumericCellValue();
    			 		break;
    			 	case 1:
    			 		//This cell contains string value
    			 		cellData=genericCell.getStringCellValue();
    			 		break;
    			 	case 2:
    			 		//This cell contains formula details
    			 		cellData=genericCell.getCellFormula();
    			 		break;
    			 	case 4:
    			 		//This cell contains boolean data
    			 		cellData=genericCell.getBooleanCellValue()?"true":"false";
    			 		break;
    			 		
    			 	default:
    			 		cellData="";
    			 		
    			 }
    		 }
    		 	
          		catch (Exception e){
          				cellData="";
						 
          		}
    	 
    	 return cellData.trim();
		}
 
    		//This method is to write in the Excel cell, Row num and Col num are the parameters
 
    	public static void setCellData(String data,int rowNum, int colNum) throws Exception	{
    			
    		FileOutputStream fileOut=null;
    	try
    	{
    		fileOut = new FileOutputStream(path_TestData);
    		
    		      	if(genericExcelWSheet.getRow(rowNum)==null) {
          				genericRow = genericExcelWSheet.createRow(rowNum);
          				
          			}
          			else
          				genericRow = genericExcelWSheet.getRow(rowNum);
       				       					 
          			genericCell = genericRow.getCell(colNum, genericRow.RETURN_BLANK_AS_NULL);
					
					if (genericCell == null) {
 
						genericCell = genericRow.createCell(colNum);
						genericCell.setCellValue("");
						genericCell.setCellValue(data);
 
						} else {
 
							genericCell.setCellValue(data);
 
						}
 
         			genericExcelWBook.write(fileOut);
    		    		
    		fileOut.flush();
  			}
    			
       		catch(Exception e)
    		{
 
				e.printStackTrace();
				throw (e);
 
			}
    	finally
    	{
    		if(fileOut != null)
    		{
    			fileOut.close();
    		}
    	}
 
				}
    		
    	public static String getSheetName(int index)
    		{
    		
    		return genericExcelWBook.getSheetName(index);
    		
    		}
    		
    	public static int getTotalSheetCount()
    		{
    			return genericExcelWBook.getNumberOfSheets();
   		}
    		
    	public static int getLastRowNumber()
    	{
    		return genericExcelWSheet.getLastRowNum(); 		
    	}
    		
    	public static int getLastColNumber()
    		{
    			return genericExcelWSheet.getRow(0).getLastCellNum();
    		}
    		
    	public static String getRow(int rowNum){
    			return genericExcelWSheet.getRow(rowNum).toString();
    		}
    		
    	public static boolean isRowEmpty(int rowNum,int exceed) { // to check invalid or blank 
    	    	genericRow = genericExcelWSheet.getRow(rowNum);    	    	
   	    	if(genericRow==null)
   	    		return (true);
   	    	
    	        for (int c = genericRow.getFirstCellNum(); c <exceed; c++) {  	        	
    	            if ( genericRow.getCell(c) == null || genericRow.getCell(c).toString().trim().equals("") || genericRow.getCell(c).getCellType()==genericCell.CELL_TYPE_BLANK)    	            	
    	                return true;
    	        }
    	        return false;
    	    }
    		
    	
    	public static void closefile()
    		{
    			try
    			{
    				FileOutputStream fileOut = new FileOutputStream(path_TestData);
    				 
    				genericExcelWBook.write(fileOut);

      				fileOut.flush();

					fileOut.close();
    			}
    			catch(Exception e)
    			{
    				e.printStackTrace();
    			}
    		}
 
    		/**
    		 * Method to connect the excel sheet
    		 * @param excelPath path of the excel sheet
    		 * @throws Exception 
    		 **/
    		
    		public static void connectExcel(String excelPath) throws Exception
    		{

    			try
    			{
	    			setExcel(excelPath);
					
    			}
    			catch(Exception e)
    			{
    				throw (e);
    			}
    		
    		}
    		
    		/**
    		 * Method  to disconnect the excel
    		 * @throws Exception 
    		 * @return void
    		 */
    		
    		public static void disconnectExcel() throws Exception
    		{

    			try
    			{
    				FileOutputStream fileOut = new FileOutputStream(path_TestData);
    				 
    				genericExcelWBook.write(fileOut);
    				genericExcelWBook.close();
      				fileOut.flush();

					fileOut.close();
    			}
    			catch(Exception e)
    			{
    				throw (e);
    			}
    		
    		}
    		
    		/**
    		 * Method used to read the each row values based on column name
    		 * @param sheetName Sheet Name of excel
    		 * @param colName column name in that excel
    		 * @return data
    		 */
    		
    		public static String getData(String sheetName,String columnName)
    		{
    			String data="";
    			try
    			{
	    			setExcelSheet(sheetName);
	    			
	    			for(int colNum=1;colNum<getLastColNumber();colNum++)
	    			{
	    				if(columnName.equals(getCellData(0, colNum)))
	    				{
	    					data=getCellData(1, colNum);
	    					
	    				}
	    			}
    			}
    			catch(Exception e)
    			{
    				e.printStackTrace();
    			}
    			
    			return data;
    		}
    		
    		/**
    		 * verify the cell value is empty
    		 * @param rowNum
    		 * @param colNum
    		 * @return
    		 */
    		public static boolean isCellEmpty(int rowNum,int colNum)
    		{
    			boolean isEmpty=false;
    			DataFormatter format = new DataFormatter();
    			String cell = format.formatCellValue(genericExcelWSheet.getRow(rowNum).getCell(colNum));
    			genericCell = genericExcelWSheet.getRow(rowNum).getCell(colNum);
    			
    			if (genericCell==null)
    			{
    				isEmpty=true;
    			}
    			else if(cell.trim().equalsIgnoreCase(""))
    			{
    				isEmpty=true;
    			}
    			return isEmpty;
    		}
	}