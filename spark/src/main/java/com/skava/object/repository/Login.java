package com.skava.object.repository;

import org.openqa.selenium.By;

public class Login {
	
	public static final By homepagecontent=By.xpath("//div[@class='header__logo-container']");
	public static final By bannercontent=By.xpath("//div[@class='uc-banner-content']");
	public static final By bannertext = By.id("uc-banner-text");
	public static final By banneraccept=By.id("uc-btn-accept-banner");
	public static final By stagewrapbutton=By.xpath("//div[@class='stage__button-wrap']");
	public static final By loginasuser=By.xpath("//img[@src='/build/logo/logo.svg']");
	public static final By logintext=By.xpath("//h1[@class='text__center']");
	public static final By loginEmailtext=By.xpath("//div[@class='form-group inputfield--required']//label[@for='email']");
	public static final By EmaidInput=By.xpath("//input[@type='email']");
	public static final By Loginpasswordtext=By.xpath("//div[@class='form__password-label']//label[@for='login__password']");
	public static final By LoginpasswordInput=By.xpath("//input[@type='password']");
	public static final By LoginSubmittext=By.xpath("//div[@class='btn-default form__login__submit form__submit  btn--submit ']//div");
	public static final By LoginSubmitButton=By.xpath("//button[@type='submit']");
	
}
