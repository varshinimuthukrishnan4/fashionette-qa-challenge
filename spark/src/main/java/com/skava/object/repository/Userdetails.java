package com.skava.object.repository;

import org.openqa.selenium.By;

public class Userdetails {
	public static final By userbutton1 = By.xpath("//span[@class='icon icon--user']");
	public static final By loginpagetext = By.xpath("//h1[@class='text__center']");
	public static final By usereditpagetext = By.xpath("//div[@class='account__welcome text__center font-size--hero']");
	public static final By MainPageTitle = By.xpath("//div[@header__logo-container");
	public static final By accordion =By.xpath("//div[@class='account-personaldata']//a[@data-component-name='accordion']");
	public static final By userdataedit = By.xpath("//a[@class='account__headline font-h1 accordion--active']");
	public static final By userpersonaldetails = By.xpath("account--customer__subheader font-h3");
	public static final By userbutton = By.xpath("//div[@class='account-personaldata']");
	public static final By updateuserinfo = By
			.xpath("(//div[@class='account--address__action account--address__action--update'])[1]");
	public static final By title = By.xpath("//input[@name='title']");
	public static final By firstname = By.xpath("//input[@name='firstName']");
	public static final By lastname = By.xpath("//input[@name='lastName']");
	public static final By emaillabeltext = By.xpath("//div[@class='account--customer__email_label']");
	public static final By editsubmit = By
			.xpath("//div[@class='account--address__action account--address__action--save']");
	
	public static final By firstnamecheck = By.xpath("//span[@class='account__firstname']");
	//div[@class='account__content__column account__content__column--customer'])[1]
	public static final By kundennummertext = By.xpath("(//div[@class='account__content__column account__content__column--customer'])[2]");
	
			public static final By personaldata = By.xpath("(//div[@class='account__content__column account__content__column--customer'])[1]");

			
			public static final By firstnamevalidate =	By.xpath("//*[contains(text(),'firstname')]");
	public static final By checkouttext1 = By.xpath("//span[@class='typography__type-2--bold']");
	
	
	public static final By checkoutpagetext1=By.xpath("//span[@class='typography__type-2--bold']");
	
	public static final By checkouttext2 = By.xpath("//*[contains(text(),'Zwischensumme')]");
	public static final By clickcoupontext = By.xpath("(//*[contains(text(),'eingeben')])[1]");
	public static final By couponinput = By.xpath("//input[@type='text']");
	public static final By submitcoupon = By.xpath("(//button[@type='submit'])[1]");
	
	public static final By carttotalbefore=By.xpath("//td[@class='text__right typography__type-2 cart__subtotal']");
	public static final By carttotalafter=By.xpath("//td[@class='text__right typography__type-2--bold']");

}
