package com.skava.reusable.components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.Logs;
import org.testng.SkipException;
import com.framework.reporting.BaseClass;
import com.framework.reporting.Log;
import com.skava.frameworkutils.ExcelReader;
import com.skava.frameworkutils.ExcelUtils;

import com.skava.object.repository.Login;
import com.skava.object.repository.Userdetails;
import com.skava.object.repository.addtocart;

import groovyjarjarantlr4.v4.semantics.UseDefAnalyzer;

public class HomePageComponents extends LoginPagecomponents {

	public void launchUrl() {

		driver.navigateToUrl(properties.getProperty("ApplicationUrl"));
		logPass("Fashionette Login Page is launched");
		if (driver.explicitWaitforVisibility(Login.loginEmailtext, "homepage title"))
			logPass("Url is successfully launched " + properties.getProperty("ApplicationUrl"));
		else
			logFail("Issue in launching the url " + properties.getProperty("ApplicationUrl"));

		
		driver.explicitWaitforVisibility(Login.bannercontent, "banner content");
		driver.jsClickElement(Login.bannercontent, "popclick");
		driver.jsClickElement(Login.bannertext, "bannertextinside");
		driver.jsClickElement(Login.banneraccept, "banneraccept");
	}

	



	

	

}