package com.skava.reusable.components;

import com.skava.frameworkutils.ExcelReader;
import com.skava.object.repository.Login;
import com.skava.object.repository.Userdetails;

public class LoginPagecomponents extends ProductListingPage {
	

	public void LoginAsValidUser() {
		String Username=ExcelReader.getData("Login","Username");
		String Password=ExcelReader.getData("Login","Password");
		
		driver.explicitWaitforVisibility(Userdetails.userbutton1, "Userbutton");
		driver.actionClickElement(Userdetails.userbutton1, "Userbutton");
		logPass("UserButton is clicked successfully to enter user details");
		driver.explicitWaitforClickable(Userdetails.loginpagetext, "LoginPageTextSuccess");
		logPass("UserLoginPage is Launched successfully");
		driver.enterText(Login.EmaidInput, Username, "EmailId");
		logPass("EmailId is entered successfully->" +Username);
		driver.explicitWaitforClickable(Login.LoginpasswordInput, "Password");
		driver.enterText(Login.LoginpasswordInput, Password, "Password");
		logPass("Password is entered successfully ->" +Password);
		driver.scrollToElement(Login.LoginSubmitButton, "LoginSubmission");
		driver.actionClickElement(Login.LoginSubmitButton, "LoginSubmission");
		logPass("User Login is Performed successfully");
		
	}

}