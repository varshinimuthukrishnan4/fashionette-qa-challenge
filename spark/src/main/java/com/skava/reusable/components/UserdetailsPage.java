package com.skava.reusable.components;

import org.openqa.selenium.By;

import com.skava.object.repository.Userdetails;
import com.skava.object.repository.addtocart;

public class UserdetailsPage extends GeneralComponents {
	public void EditUser() throws InterruptedException {
		String firstname = "Varshu" + driver.generateRandomNumber(5000);
		String lastname = "Muthu" + driver.generateRandomNumber(5000);

		driver.explicitWaitforVisibility(Userdetails.usereditpagetext, "userpageedit");

		driver.scrollToElement(Userdetails.accordion, "scrolltoaccordion");
		driver.explicitWaitforVisibility(Userdetails.accordion, "scrolltoaccordion");

		driver.jsClickElement(Userdetails.accordion, "accordion");
		logPass("Scrolled To Personal Data Section");

		driver.scrollToElement(Userdetails.kundennummertext, "kundennummertext");

		driver.explicitWaitforVisibility(Userdetails.updateuserinfo, "updateuser");
		driver.jsClickElement(Userdetails.updateuserinfo, "updateuser");

		driver.explicitWaitforVisibility(Userdetails.title, "entertitle");
		driver.enterText(Userdetails.title, "mrs", "entermrs");
		logPass("Salutation is Enetred successfully");

		driver.explicitWaitforVisibility(Userdetails.firstname, "UserFirstName");
		driver.enterText(Userdetails.firstname, firstname, "UserFirstName");
		logPass("FirstName is Enetered successfully");

		driver.enterText(Userdetails.lastname, lastname, "UserFirstName");
		logPass("LastName is Entered successfully");
		driver.jsClickElement(Userdetails.editsubmit, "submituserdetails");

		if (driver.explicitWaitforVisibility(By.xpath("//*[contains(text(),'" + firstname + "')]"), "personal data")) {

			logPass("FirstName is updated successfully: " + firstname);

		} else
			logFail("FirstName  Not updated successfully");

		if (driver.explicitWaitforVisibility(By.xpath("//*[contains(text(),'" + lastname + "')]"), "personal data")) {

			logPass("LastName is updated successfully: " + lastname);

		} else
			logFail("LastName  Not updated successfully");
	}

	public void applyvocher() {

		driver.jsClickElement(addtocart.addtocartbuttonvalidate1, "addtocartbuttonclick");
		driver.explicitWaitforClickable(Userdetails.checkouttext1, "chekouttext1");
		driver.scrollToElement(Userdetails.checkouttext1, "scrolltothecouponelement");
		driver.actionClickElement(Userdetails.clickcoupontext, "clickcoupon");
		System.out.println("coupon is clicked first pass");
		logPass("Coupon is clicked successfully");
		driver.scrollToElement(Userdetails.checkouttext2, "scrolltosubmission");
		driver.explicitWaitforClickable(Userdetails.checkouttext2, "secondcheckoutelement");
		driver.explicitWaitforClickable(Userdetails.couponinput, "waiting time");
		driver.enterText(Userdetails.couponinput, "QACHALLENGE", "couponinput");
		logPass("Coupon is enetered successfully");

		driver.actionClickElement(Userdetails.submitcoupon, "couponsubmit");

		driver.explicitWaitforVisibility(Userdetails.checkoutpagetext1, "wait for checkoutpage entry text");

		driver.jsClickElement(Userdetails.checkouttext2, "checkoutpage entry text");

		driver.explicitWaitforVisibility(Userdetails.carttotalbefore, "the sum value before");

		driver.actionClickElement(Userdetails.carttotalbefore, "the sum value before");
		String amountbefore = driver.getText(Userdetails.carttotalbefore, "the sum value before");
		amountbefore = amountbefore.split(" ")[0];
		System.out.println("amountbeforeamountbefore.....is......." + amountbefore);
		logPass("Amount before Applying voucher is->" + amountbefore);
		int val = Integer.parseInt(amountbefore);
		int value = val - 2;

		System.out.println("Value  After detection of promocode is ->" + value);
		String amountafter = driver.getText(Userdetails.carttotalafter, "the sum value after");
		amountafter = amountafter.split(" ")[0];
		int val2 = Integer.parseInt(amountafter);
		System.out.println("Value is......." + val2);
		logPass("Amount after applying voucher code is -> " + amountafter);
		if (value == val2) {
			System.out.println("offer applied successfully");
			logPass("Promocode appied successfully");
		} else {
			System.out.println("offer not applied successfully");
			logFail("Promocode not applied successfully");
		}
		logPass("Coupon Applied Successfully for the product chosen");

	}
}
