package com.skava.testcases;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.skava.reusable.components.HomePageComponents;

public class Scenario2 extends HomePageComponents {
		@BeforeClass(alwaysRun = true)
		public void initTest() throws IOException {
			driver = initiTest(this.getClass().getSimpleName());
			driver.maximizeBrowser();
		}

		@Test
		public void Scenario2() throws Exception {
	   
	     launchUrl();
	     LoginAsValidUser();
	     EditUser();
		}
		
		
		@AfterClass(alwaysRun = true)
		public synchronized void tearDown() {
			if (driver != null) 
			{
				//driver.quit();
			}
		}}

